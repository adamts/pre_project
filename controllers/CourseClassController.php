<?php

namespace app\controllers;

use Yii;
use app\models\CourseClass;
use app\models\CourseClassSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * CourseClassController implements the CRUD actions for CourseClass model.
 */
class CourseClassController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseClass models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexCourseClass')) // only team members can watch course-class 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch course-class');
		$searchModel = new CourseClassSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			//'teachers' => CourseClass::getTeachersWithAllTeachers(),    ///////////////////////////// necessary for dropdown 
			'teachers' => CourseClass::getTechersByAllRole(),    ///////////////////////////// necessary for dropdown 
			'teacher' => $searchModel->teacherId,			       ///////////////////////////// necessary for dropdown 
        ]);
    }

    /**
     * Displays a single CourseClass model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can view course-class
				throw new UnauthorizedHttpException ('Hey, You are not allowed to view course-class');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CourseClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can create course-class
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create course-class');
		$model = new CourseClass();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CourseClass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can update course-class
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update course-class');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CourseClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('crudWithoutIndex')) // only secretary's and principals can delete course-class
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete course-class');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CourseClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
