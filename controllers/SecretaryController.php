<?php

namespace app\controllers;

use Yii;
use app\models\Secretary;
use app\models\SecretarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * SecretaryController implements the CRUD actions for Secretary model.
 */
class SecretaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Secretary models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can watch secretary's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to watch secretary');
		$searchModel = new SecretarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Secretary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can view secretary's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to view secretary');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Secretary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can create secretary's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to create secretary');
		$model = new Secretary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Secretary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can update secretary's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update secretary');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Secretary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPro')) // only secretary's and principals can delete secretary's 
				throw new UnauthorizedHttpException ('Hey, You are not allowed to delete secretary');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Secretary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Secretary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Secretary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
