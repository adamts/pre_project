<?php

namespace app\controllers;

use Yii;
use app\models\Principal;
use app\models\PrincipalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * PrincipalController implements the CRUD actions for Principal model.
 */
class PrincipalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Principal models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudAdmin')) // only principals can watch principals 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to watch principals');
		$searchModel = new PrincipalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Principal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudAdmin')) // only principals can view principals 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view principals');
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Principal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudAdmin')) // only principals can create principals 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create principals');
		$model = new Principal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Principal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudAdmin')) // only principals can update principals 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update principals');
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Principal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudAdmin')) // only principals can delete principals 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete principals');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Principal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Principal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Principal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
