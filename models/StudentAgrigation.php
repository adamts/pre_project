<?php

namespace app\models;

use Yii;
use app\models\CourseClass;

/**
 * This is the model class for table "student_agrigation".
 *
 * @property integer $studentId
 * @property integer $courseNumber
 * @property integer $classNumber
 * @property integer $id
 *
 * @property CourseClass $classNumber0
 * @property CourseClass $courseNumber0
 * @property Student $student
 */
class StudentAgrigation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_agrigation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['studentId', 'courseNumber', 'classNumber'], 'integer'],
            [['classNumber'], 'exist', 'skipOnError' => true, 'targetClass' => CourseClass::className(), 'targetAttribute' => ['classNumber' => 'classNumber']],
            [['courseNumber'], 'exist', 'skipOnError' => true, 'targetClass' => CourseClass::className(), 'targetAttribute' => ['courseNumber' => 'courseNumber']],
            [['studentId'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['studentId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'studentId' => 'Student ID',
            'courseNumber' => 'Course Number',
            'classNumber' => 'Class Number',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassNumber0()
    {
        return $this->hasOne(CourseClass::className(), ['classNumber' => 'classNumber']);
    }

	////////////////////////////////////
	public function getClassNumber2()
    {
        return $this->hasOne(Classname::className(), ['classNumber' => 'classNumber']);
    }
	////////////////////////////////////
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseNumber0()
    {
        return $this->hasOne(CourseClass::className(), ['courseNumber' => 'courseNumber']);
    }
	
	///////////////////////////////
	public function getCourseNumber2()
    {
        return $this->hasOne(Course::className(), ['courseNumber' => 'courseNumber']);
    }
	///////////////////////////////
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'studentId']);
    }
}
