<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\User;

/**
 * This is the model class for table "course_class".
 *
 * @property integer $classNumber
 * @property integer $courseNumber
 * @property integer $duration
 * @property string $date
 * @property integer $teacherId
 * @property integer $id
 *
 * @property Classname $classNumber0
 * @property Course $courseNumber0
 * @property Teacher $teacher
 * @property StudentAgrigation[] $studentAgrigations
 * @property StudentAgrigation[] $studentAgrigations0
 */
class CourseClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['classNumber', 'courseNumber', 'duration', 'teacherId'], 'integer'],
            [['date'], 'safe'],
            [['classNumber'], 'exist', 'skipOnError' => true, 'targetClass' => Classname::className(), 'targetAttribute' => ['classNumber' => 'classNumber']],
            [['courseNumber'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['courseNumber' => 'courseNumber']],
            [['teacherId'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacherId' => 'id']],
			['duration', 'compare', 'compareValue' => 30, 'operator' => '>='],
			['duration', 'compare', 'compareValue' => 90, 'operator' => '<='],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'classNumber' => 'Class Number',
            'courseNumber' => 'Course Number',
            'duration' => 'Duration',
            'date' => 'Date',
            'teacherId' => 'Teacher ID',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassNumber0()
    {
        return $this->hasOne(Classname::className(), ['classNumber' => 'classNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseNumber0()
    {
        return $this->hasOne(Course::className(), ['courseNumber' => 'courseNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacherId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAgrigations()
    {
        return $this->hasMany(StudentAgrigation::className(), ['classNumber' => 'classNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAgrigations0()
    {
        return $this->hasMany(StudentAgrigation::className(), ['courseNumber' => 'courseNumber']);
    }
	//////////////////////////////////////////////////////////////////// necessary for dropdown 
	public function getTechersByRole()
	 {
			$allTeames = (new \yii\db\Query()) ////// query
           ->select(['*'])
           ->from('user')
           ->where(['userRole' => '1'])
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'id', 'username');
		return $allTeamesArray;	 
		 
	 }
	 
	public static function getTechersByAllRole()   //////////////// new function - necessary for dropdown 
	{
		$allTeachers = self::getTechersByRole();
		$allTeachers[-1] = 'All Teachers';
		$allTeachers = array_reverse ( $allTeachers, true );
		return $allTeachers;	
	}
	//////////////////////////////////////////////////////////////////////
	    public function getTeacherName()  ///////////////////////// new function
    {
        return $this->hasOne(className::User(), ['teacherId' => 'id']);
    }
	
		////////////////////////////
	public static function getTeachers()  // new function for teachers
	{
		$allTeachers = User::find()->all();
		$allTeachersArray = ArrayHelper::
					map($allTeachers, 'id', 'fullname');
		return $allTeachersArray;	
	}
	
		public static function getTeachersWithAllTeachers()   //////////////// new function - necessary for dropdown 
	{
		$allTeachers = self::getTeachers();
		$allTeachers[-1] = 'All Users';
		$allTeachers = array_reverse ( $allTeachers, true );
		return $allTeachers;	
	}
	////////////////////////////
}
