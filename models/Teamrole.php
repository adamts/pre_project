<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
//use yii\db\ActiveQuery;


/**
 * This is the model class for table "teamrole".
 *
 * @property integer $roleId
 * @property string $roleName
 *
 * @property Team[] $teams
 */
class Teamrole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teamrole';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roleId' => 'Role ID',
            'roleName' => 'Role Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['teamRole' => 'roleId']);
    }
	
	public static function getTeamroles()  
	{
		$allTeamroles = self::find()->all();
		$allTeamrolesArray = ArrayHelper::
					map($allTeamroles, 'roleId', 'roleName');
		return $allTeamrolesArray;						
	}
	
}
