<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "classname".
 *
 * @property integer $classNumber
 * @property integer $maxOfStudents
 * @property string $location
 *
 * @property CourseClass[] $courseClasses
 */
class Classname extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classname';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['maxOfStudents'], 'integer'],
			['maxOfStudents', 'compare', 'compareValue' => 10, 'operator' => '>='],
            [['location'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'classNumber' => 'Class Number',
            'maxOfStudents' => 'Max Of Students',
            'location' => 'Location',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseClasses()
    {
        return $this->hasMany(CourseClass::className(), ['classNumber' => 'classNumber']);
    }
	
	public static function getClassnamenumber()  // This function are already exist 
	{
		$allClassnamenumberes = self::find()->all();
		$allClassnamenumberArray = ArrayHelper::
					map($allClassnamenumberes, 'classNumber', 'classNumber');
		return $allClassnamenumberArray;						
	}
}
