<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student_teacher".
 *
 * @property integer $teacherId
 * @property integer $studentId
 * @property integer $id
 *
 * @property Student $student
 * @property Teacher $teacher
 */
class StudentTeacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacherId', 'studentId'], 'integer'],
            [['studentId'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['studentId' => 'id']],
            [['teacherId'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacherId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacherId' => 'Teacher ID',
            'studentId' => 'Student ID',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'studentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacherId']);
    }
	
	////////////////////////////////
	    public function getTeacherName()
    {
        return $this->hasOne(User::className(), ['id' => 'teacherId']);
    }
	////////////////////////////////
	
	////////////////////////////
	public static function getTeacherId()  // new function for teachers
	{
		$allTeacher = self::find()->all();
		$allTeacherArray = ArrayHelper::
					map($allTeacher, 'teacherId', 'id');
		return $allTeacherArray;	
	}
	
	public static function getTeacherIdWithAllTeachers()   //////////////// new function - necessary for dropdown 
	{
		$allTeacher = self::getTeacherId();
		$allTeacher[-1] = 'All Teachers';
		$allTeacher = array_reverse ( $allTeacher, true );
		return $allTeacher;	
	}
	//////////////////////////// teacher dropdown
	public function getTechersByName()
	 {
			$allTeames = (new \yii\db\Query()) ////// query
           ->select(['*'])
           ->from('user')
           ->where(['userRole' => '1'])
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'id', 'username');
		return $allTeamesArray;	 
		 
	 }
	 
	public static function getTechersByAllNames()   //////////////// new function - necessary for dropdown 
	{
		$allTeacher = self::getTechersByName();
		$allTeacher[-1] = 'All Teachers';
		$allTeacher = array_reverse ( $allTeacher, true );
		return $allTeacher;	
	}
	
	/////////////////////////// end of teacher dropdown
	
	///////////////////////////////////////// student dropdown
		public static function getStudentName()  
	{
		$allStudents = student::find()->all();
		$allStudentsArray = ArrayHelper::
					map($allStudents, 'id', 'fullname');
		return $allStudentsArray;	
	}
	
	public static function getStudentNameWithAllNames()   //////////////// new function - necessary for dropdown 
	{
		$allStudents = self::getStudentName();
		$allStudents[-1] = 'All Students';
		$allStudents = array_reverse ( $allStudents, true );
		return $allStudents;
	}
	////////////////////////////////////////// end of student dropdown
}
