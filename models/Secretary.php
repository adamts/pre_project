<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "secretary".
 *
 * @property integer $id
 * @property integer $department
 *
 * @property User $id0
 */
class Secretary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'secretary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department' => 'Department',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
