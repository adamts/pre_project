<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Classname */

$this->title = 'Create Classname';
$this->params['breadcrumbs'][] = ['label' => 'Classnames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classname-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
