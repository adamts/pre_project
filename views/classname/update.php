<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Classname */

$this->title = 'Update Classname: ' . $model->classNumber;
$this->params['breadcrumbs'][] = ['label' => 'Classnames', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->classNumber, 'url' => ['view', 'id' => $model->classNumber]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="classname-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
