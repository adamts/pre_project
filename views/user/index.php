<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Userrole;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php // css bootstrap of roles kind, done on line 33 // ?>

<img src="/yii/basic/images/users2.jpg" class="img-rounded" height="160" width="224" style="float: right;">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="user-index">
	
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-primary']) ?>


<br><br><br><br><br>
    </p>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'password',
           // 'auth_key',
            'firstname',
             'lastname',
             'email:email',
             'phone',
             //'userRole',
             //'address',
             //'notes:ntext',
             //'status',  /// status is cancelled
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
			 [
				'attribute' => 'userRole',
				'label' => 'user Role',
				'format' => 'raw',
				'value' => function($model){
					return $model->userRole0->roleName;  //////////Showing role name instead of role id.
				},
				'filter'=>Html::dropDownList('UserSearch[userRole]', $userRole, $userRoles, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			], 
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
