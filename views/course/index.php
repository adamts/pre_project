<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/yii/basic/images/courses.jpg" class="img-rounded" height="160" width="224" style="float: right;">

<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Course', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
	
<br><br><br>
		</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'courseNumber',
			[
				'attribute' => 'nameOfCourse',
				'label' => 'NameOfCourse',
				'format' => 'raw',
				'value' => function($model){
					return $model->courseItem->nameOfCourse;
				},
				'filter'=>Html::dropDownList('CourseSearch[nameOfCourse]', $nameOfCourse, $nameOfCourses, ['class'=>'form-control']),
				
			],			
   

        ],
    ]); ?>
</div>
