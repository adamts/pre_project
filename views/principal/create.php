<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Principal */

$this->title = 'Create Principal';
$this->params['breadcrumbs'][] = ['label' => 'Principals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="principal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
