<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentAgrigation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-agrigation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'studentId')->textInput() ?>

    <?= $form->field($model, 'courseNumber')->textInput() ?>

    <?= $form->field($model, 'classNumber')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
