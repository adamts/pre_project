<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StudentAgrigation */

$this->title = 'Create Student Agrigation';
$this->params['breadcrumbs'][] = ['label' => 'Student Agrigations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-agrigation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
