<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Course;
use app\models\Teacher;
use app\models\Classname;

/* @var $this yii\web\View */
/* @var $model app\models\CourseClass */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-class-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'classNumber')->dropDownList(Classname::getClassnamenumber()) ?>

    <?= $form->field($model, 'courseNumber')->dropDownList(Course::getCourse()) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

     <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <?= $form->field($model, 'teacherId')->dropDownList(Teacher::getTeacher()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
